using System.Threading.Tasks;

namespace framework
{
	
	public class Function
	{
		public static void Main()
		{
			ActualFunction fn = new ActualFunction();
			fn.Run();		
		}
	}

	class ActualFunction {

		string URL = "https://frame.work/marketplace/keyboards";
		public void Run() {
			string URL = "https://frame.work/marketplace/keyboards";
            Scraper.Scraper scraper = new Scraper.Scraper();
            bool frameworkStatus = scraper.getStatus(URL);
            System.Console.WriteLine("status: " + frameworkStatus);

            AWS.S3 s3 = new AWS.S3();
            string file = s3.ReadFile("news.rss");
            System.Console.WriteLine(file);

            Scraper.RSSUpdater rssMan = new Scraper.RSSUpdater();
            string newRss = rssMan.updateFramework(file, frameworkStatus);
    
            Task t = s3.WriteFile("news.rss", newRss);

            // ! Det måste finnas nån bättre lösning än denna, vad katten är det som disposas för tidigt!?
            System.Threading.Thread.Sleep(7000);
            
            System.Console.WriteLine("done");

			
		}
	}
}
