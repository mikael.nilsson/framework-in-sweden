# Waiting for framework in Sweden

Best way i've found so far is checking if there is a swedish keyboard for sale.
Seems like they won't release in a country without localizing the keyboard,
so I'll check if there is a swedish keyboard and update the feed based on that result.

Right now the mainline is made for running on my [fnproject server](https://fnproject.io) but my plan is to make it deployable on azure and/or AWS too, mostly to keep the cloud-fu alive.
