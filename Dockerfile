FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["framework.csproj", "."]
RUN dotnet restore "framework.csproj"
COPY . .
RUN dotnet build "framework.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "framework.csproj" -c Release -o /app

FROM base AS final
ENV FN_FORMAT=http-stream
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "framework.dll"]
