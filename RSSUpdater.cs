using System.Xml;

namespace Scraper
{
    public class RSSUpdater
    {

        XmlNode findNodeByTitle(XmlNodeList items, string title)
        {
            foreach (XmlNode elm in items)
            {
                System.Console.WriteLine(elm.SelectSingleNode("title").InnerText);
                if (elm.SelectSingleNode("title").InnerText == title)
                {
                    return elm;
                }
            }
            return null;
        }

        public string updateFramework(string file, bool newStatus)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(file);
            XmlNode root = doc.DocumentElement;
            XmlNode channel = root.SelectSingleNode("channel");
            var items = doc.GetElementsByTagName("item");

            XmlNode item = findNodeByTitle(items, "Framework Sverige");
            var descr = item.SelectSingleNode("description");
            var pubDate = item.SelectSingleNode("pubDate");
            pubDate.InnerText = System.DateTime.Now.ToString("ddd, dd MMM yyy HH’:’mm’:’ss ‘GMT");

            if (newStatus)
            {
                descr.InnerText = "YES!!!";
            }
            else
                descr.InnerText = "nix";

            return doc.OuterXml;

        }
    }
}