using System.Net;
using System.IO;

namespace Scraper
{
  class Scraper
  {
    public Scraper() { }

    public bool getStatus(string URL)
    {
      string html = scrape(URL);
      return findSwedishKeyboard(html);
    }

    bool findSwedishKeyboard(string html)
    {
      string keyboard = "Swedish";

      int keyboardIdx = html.IndexOf(keyboard);
      if (keyboardIdx > 0)
        return true;
      else return false;
    }

    string scrape(string URL)
    {
      WebRequest request = WebRequest.Create(URL);
      using WebResponse response = request.GetResponse();

      using var stream = response.GetResponseStream();
      using var reader = new StreamReader(stream);

      string data = reader.ReadToEnd();
      return data;
    }
  }
}